<?php

Auth::routes();

// Бот
Route::post('/vk_bot', 'VkBotController@messageNew');

// Домашняя страница
Route::get('/home', 'HomeController@index');

Route::get('/after', 'InvoiceController@afterlink');

// Клиенты
Route::get('/clients', 'ClientViewController@index');
Route::get('/clients/addGet', 'ClientViewController@addGet');
Route::post('/clients/addPost', 'ClientViewController@addPost');
Route::get('/clients/{client}', 'ClientViewController@details');
Route::get('/clients/{client}/editGet', 'ClientViewController@editGet');
Route::post('/clients/{client}/editPost', 'ClientViewController@editPost');
Route::get('/clients/{client}/delete', 'ClientViewController@delete');

// Заказы
Route::get('/orders', 'OrdersViewController@index');
Route::get('/orders/addGet', 'OrdersViewController@addGet');
Route::post('/orders/addPost', 'OrdersViewController@addPost');
Route::get('/orders/{order}', 'OrdersViewController@details');
Route::get('/orders/{order}/editGet', 'OrdersViewController@editGet');
Route::post('/orders/{order}/editPost', 'OrdersViewController@editPost');
Route::get('/orders/{order}/delete', 'OrdersViewController@delete');

// Инвойсы
Route::get('/invoices', 'InvoicesViewController@index');
Route::get('/invoices/{invoice}', 'InvoicesViewController@details');
Route::get('/invoices/{invoice}/delete', 'InvoicesViewController@delete');

// Терминалы
Route::get('/terminals', 'TerminalsViewController@index');
Route::get('/terminals/addGet', 'TerminalsViewController@addGet');
Route::post('/terminals/addPost', 'TerminalsViewController@addPost');
Route::get('/terminals/{terminal}', 'TerminalsViewController@details');
Route::get('/terminals/{terminal}/editGet', 'TerminalsViewController@editGet');
Route::post('/terminals/{terminal}/editPost', 'TerminalsViewController@editPost');
Route::get('/terminals/{terminal}/delete', 'TerminalsViewController@delete');

// Оплата
Route::get('/invoice/{order}', 'InvoiceController@invoice');
Route::get('/backlink', 'InvoiceController@backlink');
Route::get('/fail_backlink', 'InvoiceController@fail_backlink');
Route::post('/postlink', 'InvoiceController@postlink');
Route::get('/fail_postlink', 'InvoiceController@fail_postlink');

// Выгрузка заказов
Route::get('/order/{id}', 'TerminalController@order');
Route::get('/terminal/{terminal_id}/printed/{order}', 'TerminalController@printed');
Route::get('/terminal/{terminal}/info', 'TerminalController@info');